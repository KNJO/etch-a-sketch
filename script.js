function createGrid(length) {
    let area = length ** 2;
    let container = document.querySelector('div.container');
    let boxes = 0;
    while (boxes < area) {
        let box = document.createElement('div');
        box.classList.add('box')
        let widthHeight = (1 / length) * 100;
        box.style.width = `${widthHeight}%`;
        box.style.height = `${widthHeight}%`;
        container.appendChild(box);
        boxes++;
    }
}

function clearGrid() {
    let container = document.querySelector('div.container');
    while (container.firstChild) {
        container.removeChild(container.firstChild);
    }
}

function askForSides() {
    let sides = prompt('How many squares per side to make the new grid?');
    return sides;
}

function makeHoverable() {
    let boxes = document.querySelectorAll('div.box');
    boxes.forEach(box => box.addEventListener('mouseover', e => {
        let box = e.target;
        if (box.classList.contains('hovered')) {
            makeDarker(box);
            return;
        }

        setColor(box);
        box.style.filter = `brightness(100%)`;
        box.classList.add('hovered');
    }))
}

function makeDarker(element) {
    let currBrightness = element.style.filter;
    let regex = /\d+/g;
    let found = currBrightness.match(regex);
    // Decrease brightness by 10%
    let newBrightness = Math.floor(parseInt(found) - (10 / 100 * parseInt(found)));
    element.style.filter = `brightness(${newBrightness}%)`;
}

function setColor(element) {
    let randColor = new Array(3);
    randColor = randColor.fill(0, 0).map(() => {
        return Math.floor(Math.random() * 255);
    })
    element.style.backgroundColor = `rgb(${randColor[0]}, ${randColor[1]}, ${randColor[2]})`;
}

let length = 16;
createGrid(length);
makeHoverable();

let btn = document.querySelector('button#clear');
btn.addEventListener('click', () => {
    clearGrid();
    let side = askForSides();
    createGrid(side);
    makeHoverable();
});